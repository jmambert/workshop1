DROP DATABASE workshop;
CREATE DATABASE workshop;
\c workshop

CREATE TABLE IF NOT EXISTS workshops (
    id serial primary key,
    name text
);

CREATE TABLE IF NOT EXISTS attendants (
    firstName text,
    workshopID integer references workshops(id)
);

insert into workshops (name) values ('DevOps 101'),
                                    ('Docker Container Fundamentals'),
                                    ('Machine Learning'),
                                    ('MongoDB'),
                                    ('React Fundamentals'),
                                    ('Self-Driving Cars');


insert into attendants values ('Ahmed Abdelali',1),
                              ('Ann Frank', 2),
                              ('Ann Frank', 3),
                              ('Ann Mulkern', 1),
                              ('Ann Mulkern', 2),
                              ('Clara Weick', 3),
                              ('Clara Weick', 4),
                              ('James Archer', 6),
                              ('Linda Park', 1),
                              ('Linda Park', 2),
                              ('Linda Park', 3),
                              ('Linda Park', 4),
                              ('Linda Park', 5),
                              ('Lucy Smith', 3),
                              ('Lucy Smith', 5),
                              ('Lucy Smith', 6),
                              ('Roz Billingsley', 4),
                              ('Samantha Eggert', 1),
                              ('Samantha Eggert', 2),
                              ('Samantha Eggert', 3),
                              ('Samantha Eggert', 4),
                              ('Samantha Eggert', 5),
                              ('Samantha Eggert', 6),
                              ('Tim Smith', 2);
                             


