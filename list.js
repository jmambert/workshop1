/*global attendants*/
/*global dictionary*/


const express = require('express');
var Pool = require('pg').Pool;
var bodyParser = require('body-parser');


const app = express();
var config = {
    host: 'localhost',
    user: 'supervisor',
    password: 'ThisIsTheCompletelySecurePassword',
    database: 'workshop',
}

var pool = new Pool(config);

app.set('port', (8080));
app.use(bodyParser.json({type: 'application/json'}))
app.use(bodyParser.urlencoded({extended: true}))

Object.prototype.isEmpty = function() {
    for (var key in this) {
        if(this.hasOwnProperty(key))
            return false;
    }
    return true;
}

app.get('/api', async (req, res) => {
    try {
        if (Object.keys(req.query).length === 0) {
            var anything = req.query.workshops;
            var response = await pool.query('select name from workshops');
            for (var i = 0; i < response.length; i++) {
            for (var key in response[i]) {
                if (response[i].hasOwnProperty(key)) {
                    console.log(response[i][key]);
                    }
                }
            }
        var listToPrint = new Array();
        response.rows.forEach( function (arrayItem) {
            listToPrint.push(arrayItem["name"]);
        });
        var dict = {
            workshops: listToPrint
        };
        res.json (dict);
        } else {
        var anything = req.query.workshop;
        var response = await pool.query('select a.firstname from attendants a join workshops w on a.workshopID = w.id where w.name = $1', [anything]);
        for (var i = 0; i < response.length; i++) {
        for (var key in response[i]) {
            if (response[i].hasOwnProperty(key)) {
                console.log(response[i][key]);
                }
            }
        }
        if (response.rows.isEmpty()) {
            res.json('error: workshop not found')
        } else {
        var listToPrint = new Array();
        response.rows.forEach( function (arrayItem) {
            listToPrint.push(arrayItem["firstname"]);
        });
             var dict = {
            attendees: listToPrint
        };
        res.json (dict);
            }
        }
    } catch(e) {
        console.error('Error running query $1 ', e.message)
    }
    
});

app.post('/api', async (req, res) => {
    var attendants = req.body.attendants
    var workshop = req.body.workshop;
    if (!attendants || !workshop) {
        res.json({error: 'parameters not given'});
    } else {
        var check = await pool.query('select a.firstname from attendants a join workshops w on a.workshopID = w.id where w.name = $1', [workshop]);
        if(check.rows.isEmpty()) {
            var table = await pool.query('insert into workshops (name) values ($1)', [workshop]);
            var person = await pool.query('insert into attendants (firstname, workshopid) values (($1),(SELECT id from workshops where name = ($2)))', [attendants, workshop]);
            var dict = {
                attendee: attendants,
                workshop: workshop
            }
            res.json(dict);
        } else {
            var checkers = await pool.query('select a.firstname from attendants a join workshops w on a.workshopID = w.id where a.firstname = $1 AND a.workshopID = (SELECT id from workshops WHERE name = ($2))', [attendants, workshop]);
            if (checkers.rows.isEmpty()) {
                var person = await pool.query('insert into attendants (firstname, workshopid) values (($1),(SELECT id from workshops where name = ($2)))', [attendants, workshop]);
                var dict = {
                    attendee: attendants,
                    workshop: workshop
                }
                res.json(dict);
            } else {
                res.json({error: 'attendee already enrolled'});
            }
        }
    }
});

app.listen(app.get('port'), () => {
    console.log('Running')
})